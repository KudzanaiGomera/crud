<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

//include headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=UTF-8");

//including files
include_once("../config/db.php");

include_once("../classes/Crud.php");

//objects
$db = new Database();

$connection = $db->connect();

$read = new Crud($connection);

if($_SERVER['REQUEST_METHOD'] === 'GET'){

    $records = $read->read();

    if($records->num_rows > 0){

        $records_arr = array();

        while($row = $records->fetch_assoc()){
           
            $records_arr[] = array(
                'id' => $row['id'],
                'name' => $row['name'],
                'email' => $row['email'],
                'address' => $row['address'],
                'mobile' => $row['mobile'],
                'created_at' => $row['created_at'],
            );
        }
        http_response_code(200); // Ok
        echo json_encode(array(
            "status" => 1,
            "records" => $records_arr,
            "message" => "records retrieved successfully"
        ));
    }else{
        http_response_code(404); // no data found
        echo json_encode(array(
            "status" => 0,
            "message" => "No Records Found"
        ));
    }
}
?>