<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

//include CORS headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Max-Age: 3628800");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

//including files
include_once("../config/db.php");

include_once("../classes/Crud.php");

//objects
$db = new Database();

$connection = $db->connect();

$user_obj = new Crud($connection);

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $data = json_decode(file_get_contents("php://input"));

    if(!empty($data->name) && !empty($data->email) && !empty($data->address) && !empty($data->mobile)){

        $user_obj->name = $data->name;
        $user_obj->email = $data->email;
        $user_obj->address = $data->address;
        $user_obj->mobile = $data->mobile;

        $email_data = $user_obj->check_email();

        if(!empty($email_data)){
            //some data we have - insert should not go
            http_response_code(500);
            echo json_encode(array(
                "status" => 0,
                "error" => "Record already exists, try another email address"
            ));
        }
        else{
            if($user_obj->create()){
                http_response_code(200);
                echo json_encode(array(
                    'status' => 1,
                    "message" => "Record has been created"
                ));
            }
        }
    }
    else{
        http_response_code(500);
        echo json_encode(array(
            'status' => 0,
            "error" => "All data needed"
        ));
    }
}
// for everything that isn't a POST request 
else{
    http_response_code(200);
    echo json_encode(array(
        'status' => 1,
        "message" => "Option Not Allowed"
    ));
}

?>