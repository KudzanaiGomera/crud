<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

//include CORS headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Max-Age: 3628800");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");

//debug here
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

//including files
include_once("../config/db.php");

include_once("../classes/Crud.php");

//objects
$db = new Database();

$connection = $db->connect();

$record_obj = new Crud($connection);

if($_SERVER['REQUEST_METHOD'] === 'POST'){

    if(!empty($_REQUEST['selectedRecord']) && !empty($_REQUEST['name']) && !empty($_REQUEST['email']) && !empty($_REQUEST['address']) && !empty($_REQUEST['mobile'])){

        $record_obj->id = $_REQUEST['selectedRecord'];
        $record_obj->name = $_REQUEST['name'];
        $record_obj->email = $_REQUEST['email'];
        $record_obj->address = $_REQUEST['address'];
        $record_obj->mobile = $_REQUEST['mobile'];

        
        if($record_obj->update()){
            http_response_code(200);
            echo json_encode(array(
                'status' => 1,
                "message" => "Record has been updated"
            ));
            
        }
    }
    else{
        http_response_code(500);
        echo json_encode(array(
            'status' => 0,
            "error" => "All data needed"
        ));
    }
}
// for everything that isn't a POST request 
else{
    http_response_code(200);
    echo json_encode(array(
        'status' => 1,
        "message" => "Option Not Allowed"
    ));
}

?>