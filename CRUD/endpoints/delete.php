<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

//include CORS headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Max-Age: 3628800");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");

//debug here
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

//including files
include_once("../config/db.php");

include_once("../classes/Crud.php");

//objects
$db = new Database();

$connection = $db->connect();

$delete_obj = new Crud($connection);

if($_SERVER['REQUEST_METHOD'] === 'POST'){

    //body
    $data = json_decode(file_get_contents("php://input"));

    if(!empty($data->id)){
        
        $delete_obj->id = $data->id;
        
        $delete_obj->delete();
    }
    else{
        http_response_code(404); //not found
        echo json_encode(array(
            "status" => 0,
            "message" => "All data needed"
            
        ));
    }
}

// for everything that isn't a POST request 
else{
    http_response_code(200);
    echo json_encode(array(
        'status' => 1,
        "message" => "Option Not Allowed"
    ));
}
?>